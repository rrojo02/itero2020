<?php

use Illuminate\Support\Facades\Route;
use Laravel\Jetstream\Http\Controllers\Livewire\UserProfileController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/health', function () {
    config()->set('session.driver', 'array');
    return response('Healthy', 200)
        ->header('Content-Type', 'text/plain');
});

Route::middleware(['portal'])->get('/welcome', function () {
    return view('portal.welcome');
})->name('welcome');

Route::middleware(['portal', 'auth:trainee'])->get('/help', function () {
    return view('portal.help-dashboard');
})->name('help');

Route::middleware(['portal', 'auth:trainee'])->get('/', function () {
    return view('portal.index');
})->name('portal');

Route::group(['middleware' => config('jetstream.middleware', ['web'])], function () {
    Route::group(['middleware' => ['auth:web', 'verified']], function () {

        Route::prefix('admin')->group(function() {

            Route::get('/dashboard', function () {
                return view('dashboard');
            })->name('dashboard');

            // User & Profile...
            Route::get('/user/profile', [UserProfileController::class, 'show'])
                ->name('profile.show');
        });
    });
});
