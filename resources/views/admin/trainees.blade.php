<table id="usersTable" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
    <thead>
    <tr>
        <th data-priority="1">First Name</th>
        <th data-priority="1">Last Name</th>
        <th data-priority="2">Group</th>
        <th data-priority="2">Language</th>
    </tr>
    </thead>
    <tbody>
    @foreach($trainees as $key => $trainee)
        <tr>
            <td>{{ $trainee->first_name }}</td>
            <td>{{ $trainee->last_name }}</td>
            <td>{{ $trainee->group }}</td>
            <td>{{ $trainee->language }}</td>
        </tr>
    @endforeach
    </tbody>

</table>
