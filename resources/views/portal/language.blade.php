<div class="collapse navbar-collapse justify-content-end" id="navigation">

    <ul class="navbar-nav">

        <li class="nav-item btn-rotate dropdown">
            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="nc-icon nc-world-2"></i>
                <p>
                    @lang('training.'.$livewireLocale)
                </p>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                @foreach($livewireLocales as $locale)
                    <a class="dropdown-item" wire:click.prevent="setLocale('{{$locale}}')" role="button" href="#">@lang('training.'.$locale)</a>
                @endforeach
            </div>
        </li>
    </ul>
</div>