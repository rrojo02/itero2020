
@if (count($messages) > 0)
    <div class="row">
        <div class="col-md-12">
            <div class="card card-chart">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6 ml-auto mr-auto text-center">
                            <h5 class="card-title">{{ __('training.training_audio') }}</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach($messages as $key => $message)
                            <div class="ml-1">
                            </div>
                            <div class="ml-3">
                                <h6>{{ __('training.message') . ' ' . ($key + 1) }}</h6>
                                <audio controls>
                                    <source src="{{$message->path}}" type="audio/mpeg">
                                    Your browser does not support the audio tag.
                                </audio>
                            </div>
                            <div class="ml-1">
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="card-footer">
                </div>
            </div>
        </div>
    </div>
@endif