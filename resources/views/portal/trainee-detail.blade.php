<div class="card-body ">
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <label>{{ __('training.username') }}</label>
                <input type="text" class="form-control" disabled=""  value="{{ $trainee->user_name }}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>{{ __('training.name') }}</label>
                <input type="text" class="form-control" disabled="" value="{{ $trainee->first_name }} {{ $trainee->last_name }}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>{{ __('training.group') }}</label>
                <input type="text" class="form-control" disabled=""  value="{{ $trainee->group }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="form-group">
                <label for="exampleInputEmail1">{{ __('training.email') }}</label>
                <input type="text" disabled="" class="form-control" value="{{ $trainee->email }}">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>{{ __('training.language') }}</label>
                <input type="text" class="form-control" disabled="" value="{{ $trainee->language }}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>{{ __('training.city') }}</label>
                <input type="text" class="form-control" disabled="" value="-">
            </div>
        </div>
    </div>
</div>