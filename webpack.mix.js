const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
    'resources/js/app.js',
    'resources/js/bootstrap.js',
    'resources/js/paper-dashboard.js'
], 'public/js/app.js');

mix.postCss('resources/css/app.css', 'public/css', [
    require('postcss-import'),
    require('tailwindcss'),
]);

mix.styles([
    'resources/css/bootstrap.min.css',
    'resources/css/paper-dashboard.css'
], 'public/css/style.css');
