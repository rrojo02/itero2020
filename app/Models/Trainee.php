<?php


namespace App\Models;

use Grosv\LaravelPasswordlessLogin\Traits\PasswordlessLogin;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Trainee extends Authenticatable
{
    use PasswordlessLogin;

    protected $guard = 'trainee';

    protected $hidden = [
        'password',
    ];
}