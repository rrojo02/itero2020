<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Arr;
use Psr\Http\Message\ResponseInterface;

class TinyUrlShortener
{
    protected $client;
    protected const defaults = [
        'allow_redirects' => false,
        'base_uri' => 'https://tinyurl.com',
    ];

    /**
     * Create a new TinyURL shortener.
     *
     * @param \GuzzleHttp\Client $client
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * {@inheritDoc}
     */
    public function shortenAsync($url, array $options = [])
    {
        $options = array_merge(Arr::add(static::defaults, 'query.url', $url), $options);
        $request = new Request('GET', '/api-create.php');

        return $this->client->sendAsync($request, $options)->then(function (ResponseInterface $response) {
            return str_replace('http://', 'https://', $response->getBody()->getContents());
        })->wait();
    }
}