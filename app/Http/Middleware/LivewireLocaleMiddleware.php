<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class LivewireLocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has('locale')){
            $livewireLocale = session()->get('locale');

            app()->setLocale($livewireLocale);
            session()->put('locale', $livewireLocale);
        } else {
            $user = Auth::user();
            $locale = isset($user) && $user->language === 'German' ? 'de' : 'en';
            app()->setLocale($locale);
            session()->put('locale', $locale);
        }

        return $next($request);
    }
}