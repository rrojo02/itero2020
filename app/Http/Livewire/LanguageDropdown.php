<?php


namespace App\Http\Livewire;

use Livewire\Component;

class LanguageDropdown extends Component
{
    use LivewireLocaleTrait;

    public function setLocale(string $local)
    {
        app()->setLocale($local);
        session()->put('locale', $local);

        return redirect()->to('/');
    }

    public function render()
    {
        return view('portal.language');
    }
}