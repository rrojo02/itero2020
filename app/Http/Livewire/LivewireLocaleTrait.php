<?php

namespace App\Http\Livewire;


trait LivewireLocaleTrait
{
    public $livewireLocale;

    public $livewireLocales;

    public function mount()
    {
        $this->livewireLocale = app()->getLocale();
        $this->livewireLocales = config('app.locales');
    }
}