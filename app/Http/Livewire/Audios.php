<?php


namespace App\Http\Livewire;

use App\Models\Message;
use Livewire\Component;

class Audios extends Component
{
    public function render()
    {
        return view('portal.audios', [
            'messages' => Message::query()->whereActive(1)->get()
        ]);
    }
}