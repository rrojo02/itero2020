<?php

namespace App\Http\Livewire;

use Livewire\Component;

class HelpDashboard extends Component
{
    public function render()
    {
        return view('layout.help-dashboard');
    }
}
