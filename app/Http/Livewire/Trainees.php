<?php


namespace App\Http\Livewire;

use App\Models\Trainee;
use Livewire\Component;

class Trainees extends Component
{
    public function render()
    {
        return view('admin.trainees', [
            'trainees' => Trainee::query()->get()
        ]);
    }
}