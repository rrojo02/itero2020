<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class TraineeDetail extends Component
{
    public function render()
    {
        return view('portal.trainee-detail', [
            'trainee' => Auth::user()
        ]);
    }
}
