<?php

namespace App\Console\Commands;

use App\Models\Trainee;
use Illuminate\Console\Command;
use Grosv\LaravelPasswordlessLogin\LoginUrl;

class GenerateMagicLink extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:link';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command creates a magic link for trainees table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $trainees = Trainee::query()->get();

        foreach ($trainees as $trainee) {
            $generator = new LoginUrl($trainee);
            $generator->setRedirectUrl('/');
            $url = $generator->generate();

            $trainee->link = $url;
            $trainee->save();
        }

        return 0;
    }
}
